<h1><b>Final Project</b></h1>

## Grup 2 - Kelompok 11

## Anggota Kelompok

- Ahmad Ali Fahmi @alifahmi
- Anggara Wahyu Dwiatmaja @AWD777

## Tema Project

Database Gunung Api di Indonesia

## ERD

![laravel-project1_ERD.png](./laravel-project1_ERD.png)

## Link Video

https://www.youtube.com/watch?v=Mz5YsT4GBWA

## Link Deploy

http://dbgunung.herokuapp.com/
