<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
Use Illuminate\Support\Facades\Auth;
use App\User;
use Alert;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::id())->first();
        return view('/account', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($id)],
            'institusi' => 'required', 
            'alamat' => 'required',
        ]);

        $user = User::find($id);
 
        $user->nama = $request['nama'];
        $user->email = $request['email'];
        $user->institusi = $request['institusi'];
        $user->alamat = $request['alamat'];

        if ($request['password_confirmation'] === $request['password'])
        {
            if ($request['password'] != '')
            {            
                $request->validate([
                    'password' => ['string', 'min:8']
                ]);
                $user->password = Hash::make($request['password']);    
            }
            
            $user->save();
            Alert::success('Updated', 'Acoount Berhasil Diupdate');
        }
        else 
        {
            Alert::error('Failed', 'Password Tidak Sesuai');
        }
                   
        return redirect('/account');
    }

}
