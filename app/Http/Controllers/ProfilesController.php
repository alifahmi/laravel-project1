<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profiles;
use App\Volcanos;
use App\Sensor;
use App\VolcanoSensor;
use Alert;
use File;

class ProfilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sensor = Sensor::find($id);
        $volcanosensor = VolcanoSensor::find($id);
        $volcano = Volcanos::find($id);
        $profile = Profiles::find($id);
        return view('profile.show', compact('sensor','volcanosensor','volcano','profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profiles::find($id);
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'alamat' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'elevation' => 'integer',
        ]);
        $profile = Profiles::find($id);
        if($request->has('foto'))
        {
            $posterFileName = time() . "." . $request->foto->extension();
            $request->file('foto')->move('img', $posterFileName);
            $valuefoto=$request->foto;
            $profile->foto = $posterFileName;
        }
        
        
        $profileinfo=$profile->volcanos->nama;
        $profile->alamat = $request->alamat;
        $profile->latitude = $request->latitude;
        $profile->longitude = $request->longitude;
        $profile->elevation = $request->elevation;
        
        // if($valuefoto!=""){
            
        //     $profile->foto = $fotoFileName;
        // }
        
        $profile->save();
        Alert::success('Edit Profil Gunungapi', 'Profil Gunungapi '.$profileinfo.' Berhasil Diedit');
        return redirect('/volcano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
