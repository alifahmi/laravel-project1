<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sensor;
use App\Volcanos;
use Alert;

class SensorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sensors = Sensor::all();
        return view('sensors.index', compact('sensors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sensors = Sensor::all();
        return view('sensors.create', compact('sensors'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jenis' => 'required',
            'merk' => 'required',
            'fungsi' => 'required',
        ]);

        $sensor = new Sensor;
 
        $sensor->jenis = $request->jenis;
        $sensor->merk = $request->merk;
        $sensor->fungsi = $request->fungsi;

        $sensor->save();

        Alert::success('Tambah Sensor', 'Sensor Berhasil Ditambahkan');
        return redirect('/sensors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sensor = Sensor::find($id);
        $volcano=Volcanos::all();
        return view('sensors.show', compact('sensor','volcano'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sensors = Sensor::where('id', $id)->first();
        return view('sensors.edit', compact('sensors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis' => 'required',
            'merk' => 'required',
            'fungsi' => 'required',
        ]);

        $sensor = Sensor::find($id);
 
        $sensor->jenis = $request['jenis'];
        $sensor->merk = $request['merk'];
        $sensor->fungsi = $request['fungsi'];
        
        $sensor->save();
        Alert::success('Edit Sensor', 'Sensor Berhasil Diedit');
        return redirect('/sensors');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sensor = Sensor::find($id);
 
        $sensor->delete();
        Alert::success('Hapus Sensor', 'Sensor Berhasil Dihapus');
        return redirect('/sensors');
    }
}
