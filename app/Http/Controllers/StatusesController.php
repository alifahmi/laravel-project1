<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statuses;
use App\Volcanos;
use Alert;

class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = Statuses::all();
        return view('status.index', compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Statuses::all();
        return view('status.create', compact('status'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status' => 'required',
            'info' => 'required',
        ]);

        $status = new Statuses;
 
        $status->status = $request->status;
        $status->info = $request->info;

        $status->save();

        Alert::success('Tambah Status', 'Status Berhasil Ditambahkan');
        return redirect('/status');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = Statuses::findOrfail($id);
        $volcano = Volcanos::all();
        return view('status.show', compact('status', 'volcano'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Statuses::find($id);
        return view('status.edit', compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'required',
            'info' => 'required',
        ]);

        $status = Statuses::find($id);
        $statusinfo=$status->status;
        $status->status = $request->status;
        $status->info = $request->info;
        
        $status->save();
        Alert::success('Edit Status', 'Status '.$statusinfo.' Berhasil Diedit');
        return redirect('/status');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Statuses::find($id);
        $statusinfo=$status->status;
        $status->delete();
        Alert::success('Status '.$statusinfo, 'Status '.$statusinfo.' Berhasil Dihapus');
        return redirect('/status');
    }
}
