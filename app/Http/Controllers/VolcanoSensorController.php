<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\VolcanoSensor;
use App\Volcanos;
use Alert;

class VolcanoSensorController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'volcanos_id' => 'required',
            'sensor_id' => 'required',
            'year' => 'required', 
            'status' => 'required',
        ]);

        $volcanosensor = new VolcanoSensor;
 
        $volcanosensor->volcanos_id = $request['volcanos_id'];
        $volcanosensor->sensor_id = $request['sensor_id'];
        $volcanosensor->year = $request['year'];
        $volcanosensor->status = $request['status'];

        $volcanosensor->save();

        return redirect('/volcano');
    }
    
    public function destroy($id)
    {
        $volcanosensor = VolcanoSensor::find($id);
        $volcanosensor->delete();

        Alert::success('Hapus Sensor', 'Sensor Berhasil Dihapus');
        return redirect('/volcano');
    }
}
