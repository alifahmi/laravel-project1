<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Volcanos;
use Alert;
use App\Statuses;
use App\Profiles;
use App\Sensor;
use App\VolcanoSensor;
use DB;
use File;

class VolcanosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profile = Profiles::all();
        $volcano = Volcanos::all();
        return view('volcano.index', compact('profile','volcano'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $status = Statuses::all();
        $volcano = Volcanos::all();
        $sensor = Sensor::all();
        return view('volcano.create', compact('status','volcano', 'sensor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'status' => 'required',
            'sejarah' => 'required',
        ]);

        $volcano = new Volcanos;
        $namatemp=$request->nama;
        $volcano->nama = $namatemp;
        $volcano->statuses_id = $request->status;
        $volcano->sejarah = $request->sejarah;
        $volcano->save();

        $volcanotemp = DB::table('volcanos')
                    ->where('nama', '=', $namatemp)
                    ->orderByRaw('created_at DESC')
                    ->get();

        $profile = new Profiles;
        $profile->alamat = "None";
        $profile->latitude = "None";
        $profile->longitude = "None";
        $profile->elevation = 0;
        $profile->foto = "no_image.jpg";
        $profile->volcanos_id = $volcanotemp[0]->id;
        $profile->save(); 	
        

        Alert::success('Tambah Gunungapi', 'Gunungapi Berhasil Ditambahkan');
        return redirect('/volcano');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $volcanosensor=VolcanoSensor::find($id);
        $volcano = Volcanos::find($id);
        $sensor = Sensor::all();
        return view('volcano.show', compact('volcanosensor','volcano', 'sensor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Statuses::all();
        $volcano = Volcanos::find($id);
        return view('volcano.edit', compact('status','volcano'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'status' => 'required',
            'sejarah' => 'required',
        ]);

        $volcano = Volcanos::find($id);
        $volcanoinfo=$volcano->nama;
        $volcano->nama = $request->nama;
        $volcano->statuses_id = $request->status;
        $volcano->sejarah = $request->sejarah;
        
        $volcano->save();
        Alert::success('Edit Gunungapi', 'Gunungapi '.$volcanoinfo.' Berhasil Diedit');
        return redirect('/volcano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profiles::where('volcanos_id', '=', $id)->first();
        $path = "img/";
        File::delete($path.$profile->foto);

        $volcano = Volcanos::find($id);
        $volcanoinfo=$volcano->nama;
        $volcano->delete();
        
        Alert::success('Gunungapi '.$volcanoinfo, 'Gunungapi '.$volcanoinfo.' Berhasil Dihapus');
        return redirect('/volcano');
    }
}
