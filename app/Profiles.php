<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profiles extends Model
{
    protected $table = 'profiles';
    protected $fillable = ['alamat','latitude','longitude','elevation','foto','volcanos_id' ];

    public function volcanos()
    {
        return $this->belongsTo('App\Volcanos');
    }
}
