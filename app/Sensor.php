<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Alert;

class Sensor extends Model
{
    protected $table = 'sensors';
    protected $fillable = ['jenis', 'merk', 'fungsi'];

    public function volcanosensor()
    {
        return $this->hasMany('App\VolcanoSensor');
    }
}
