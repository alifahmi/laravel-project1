<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statuses extends Model
{
    protected $table = 'statuses';
    protected $fillable = ['status', 'info'];

    public function volcanos()
    {
        return $this->hasMany('App\Volcanos');
    }
}
