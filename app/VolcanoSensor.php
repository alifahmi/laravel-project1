<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VolcanoSensor extends Model
{
    protected $table = 'volcanosensor';
    protected $fillable = ['volcanos_id', 'sensor_id', 'year', 'status','id'];

    public function volcano()
    {
        return $this->belongsTo('App\Volcano');
    }

    public function sensor()
    {
        return $this->belongsTo('App\Sensor');
    }
        
}
