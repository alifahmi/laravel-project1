<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Volcanos extends Model
{
    protected $table = 'volcanos';
    protected $fillable = ['nama', 'statuses_id', 'sejarah'];

    public function statuses()
    {
        return $this->belongsTo('App\Statuses');
    }

    public function profiles()
    {
        return $this->hasOne('App\Profiles');
    }

    public function volcanosensor()
    {
        return $this->hasMany('App\VolcanoSensor');
    }
    
}
