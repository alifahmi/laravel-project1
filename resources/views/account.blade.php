@extends('layouts.master')

@section('judul')
    Account Details
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/u10xbgftgiw6lcaak096p8obsz15qhs4uq1vpkkm2ohq0qao/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinycomments tinymcespellchecker',
    //   toolbar: 'a11ycheck addcomment showcomments casechange checklist code export pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>
@endpush

@section('content')

<script>
function showPass() {
    var x = document.getElementById("passInput");
    if (x.type === "password") {
      x.type = "text";
    } else {
      x.type = "password";
    }
  }
</script>

<form action="/account/{{$user->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="form-group">
        <label>Nama Lengkap</label>
        <input type="text" name="nama" value="{{$user->nama}}" class="form-control">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" name="email" value="{{$user->email}}" class="form-control">
    </div>
    @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="New Password" id="passInput">
        <input type="checkbox" onclick="showPass()"> Show Password
        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm New Password" id="passInput">
    </div>
    @error('password')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label>Institusi</label>
        <input type="text" name="institusi" value="{{$user->institusi}}" class="form-control">
    </div>
    @error('institusi')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control">{{$user->alamat}}</textarea>
    </div>
    @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Update</button>
    <a href="/" class="btn btn-info mb-sm ml-2">Back</a>
</form>

@endsection