@extends('layouts.master')

@section('judul')
    Selamat Datang
@endsection

@section('content')
    <h1>Database Gunung Indonesia</h1>
    <div id="googleMap" style="width:100%;height:400px;"></div>

    <script>
    function myMap() {
    var mapProp= {
    center:new google.maps.LatLng(0.14891937971476094, 116.57528808048441),
    zoom:5,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
    }
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=&callback=myMap"></script>
@endsection