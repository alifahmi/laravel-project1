<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('AdminLTE/dist/img/mountain2.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">DBGunung</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('AdminLTE/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          @guest
              <a href="/login" class="d-block">Belum Login</a>
          @endguest

          @auth
            <a href="/account" class="d-block">{{ Auth::user()->nama}} ({{ Auth::user()->institusi}})</a>
          @endauth
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            <li class="nav-item">
                <a href="/" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Home
                    </p>
                </a>
            </li>
            
            @auth
            <li class="nav-item">
              <a href="/account" class="nav-link">
                <i class="nav-icon fas fa-user"></i>
                <p>
                  User Account
                </p>
              </a>
            </li>
            @endauth


            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-table"></i>
                <p>
                  Categories
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="/sensors" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>Sensor</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/status" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>Status</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="/volcano" class="nav-link">
                    <i class="fas fa-circle nav-icon"></i>
                    <p>
                      Volcano
                    </p>
                  </a>
                </li>


                </li>
              </ul>
              
            </li>

            @auth
            <li class="nav-item">
              <a class="nav-link bg-danger" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <i class="nav-icon fas fa-sign-out" aria-hidden="true"></i>
                  <p>
                    Logout
                  </p>
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
            </li>
            @endauth

            @guest
            <li class="nav-item">
              <a href="/login" class="nav-link bg-primary">
                <i class="fas fa-sign-in"></i>
                <p>
                  Login
                </p>
              </a>
            </li>
            @endguest

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>