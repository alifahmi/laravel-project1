@extends('layouts.master')

@section('judul')
    Lengkapi Profile Gunungapi
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/r76ebw8jiapbixdiisux58gj9qoju0jfjmfzsanvsbu1fthm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>    
<script>
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
</script>
@endpush

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')

<form action="/profile/{{$profile->id}}" method="POST" enctype="multipart/form-data">
  @csrf
  @method('PUT')
    <div class="form-group">
      <label>Gunungapi</label>
      <input type="text" class="form-control" value="{{$profile->volcanos->nama}}" readonly>
    </div>

    <div class="form-group">
      <label>Alamat</label>
      <textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    	 	 	 	
    <div class="form-group">
      <label>Latitude</label>
      <input type="text" name="latitude" class="form-control" value="{{$profile->latitude}}">
    </div>
    @error('latitude')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Longitude</label>
      <input type="text" name="longitude" class="form-control" value="{{$profile->longitude}}">
    </div>
    @error('longitude')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Elevation</label>
      <input type="text" name="elevation" class="form-control" value="{{$profile->elevation}}">
    </div>
    @error('elevation')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <div class="row">
        <div class="col-3">
          <img src="{{asset('img/' . $profile->foto)}}" alt="img/no_image_data.jpg" width="200" height="150" class="rounded" style="border:2px solid black;">
        </div>
        <div class="col-9">
          <label>Foto</label>
          <input type="file" name="foto" class="form-control" accept="image/png, .jpeg, .jpg, image/gif">
        </div>
      </div>
    </div>
    @error('foto')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="d-flex justify-content-end">
      <button type="submit" class="btn btn-primary mx-2">Update</button>
      <a href="/volcano" class="btn btn-primary mb-sm">Back</a>
    </div>
  </form>
  
  
  @endsection