@extends('layouts.master')
@section('judul')
    <p>Detail Data Gunungapi</p>
@endsection
    
@section('content')
    <div class="d-flex justify-content-center"">
        <h4>Detail Data Gunungapi {{$profile->volcanos->nama}}</h4>
    </div>
    <div class="row my-1">
        <div class="col">
            <div class="card ">
                <div class="container mx-1 my-2 rounded">
                    <img src="{{asset('img/' . $profile->foto)}}" alt="img/no_image_data.jpg" width="500" height="400" class="rounded">
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="container mx-1 my-2 rounded">
                    <table class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <th style="width: 18px">Nama</th>
                                <td>{{$profile->volcanos->nama}}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>{{$volcano->statuses->status}}</td>
                            </tr>
                            <tr>
                                <th>Latitude</th>
                                <td>{{$profile->latitude}}</td>
                            </tr>
                            <tr>
                                <th>Longitude</th>
                                <td>{{$profile->longitude}}</td>
                            </tr>
                            <tr>
                                <th>Elevation</th>
                                <td>{{$profile->elevation}}</td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td>{{strip_tags($profile->alamat)}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container-fluid mx-1 my-2">
            <div class="card rounded">
                <div class="card-header">
                    <h3>Status {{$volcano->statuses->status}}</h3>
                </div>
                <div class="card-body">
                    {{strip_tags($volcano->statuses->info)}}
                </div>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="container-fluid mx-1 my-2">
            <div class="card rounded">
                <div class="card-header">
                    <h3>Sensor yang dipasang</h3>
                </div>
                <div class="card-body">
                    <table class="table" id="tabelsensor" class="table table-bordered table-striped">
                        <thead class="thead-light">
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Jenis</th>
                            <th scope="col">Merk</th>
                            <th scope="col">Fungsi</th>
                            <th scope="col">Tahun</th>
                            <th scope="col">Aktif</th>
                          </tr>
                        </thead>
                        <tbody>
                            
                         @forelse ($volcano->volcanosensor as $key => $volcano1)
                            <tr>
                                <td>{{$key + 1}}</td>
                                <td>{{$volcano1->sensor->jenis}}</td>
                                <td>{{$volcano1->sensor->merk}}</td>
                                <td>{{$volcano1->sensor->fungsi}}</td>
                                <td>{{$volcano1->year}}</td>
                                @if ($volcano1->status=="1")
                                    <td>Aktif</td>
                                @else
                                    <td>Tidak Aktif</td>
                                @endif
                            </tr>
                         @empty
                         <h1>Data tidak ditemukan</h1>
                             
                         @endforelse
                        </tbody>
                      </table>
                </div>
                </div>
        </div>
    </div>

    <div class="row">
        <div class="container-fluid mx-1 my-2">
            <div class="card rounded">
                <div class="card-header">
                    <h3>Sejarah</h3>
                </div>
                <div class="card-body">
                    {{strip_tags($profile->volcanos->sejarah)}}
                </div>
                </div>
        </div>
    </div>

    <br>
    <div class="d-flex justify-content-end"">
        <a href="/volcano" class="btn btn-primary mb-sm">Back</a>
    </div>
@endsection