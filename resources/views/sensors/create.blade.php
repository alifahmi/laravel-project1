@extends('layouts.master')

@section('judul')
    Tambah Sensor
@endsection

@section('content')

<form action="/sensors" method="POST">
    @csrf
    
    <div class="form-group">
      <label>Jenis Sensor</label>
      <input type="text" name="jenis" class="form-control">
    </div>
    @error('jenis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Merk</label>
      <input type="text" name="merk" class="form-control">
    </div>
    @error('merk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Fungsi</label>
      <input type="text" name="fungsi" class="form-control">
    </div>
    @error('fungsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  
  @endsection