@extends('layouts.master')

@section('judul')
    Edit Sensor
@endsection

@section('content')

<form action="/sensors/{{$sensors->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
    <label>Jenis Sensor</label>
    <input type="text" name="jenis" value="{{$sensors->jenis}}" class="form-control">
    </div>
    @error('jenis')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Merk</label>
    <input type="text" name="merk" value="{{$sensors->merk}}" class="form-control">
    </div>
    @error('merk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Fungsi</label>
    <input type="text" name="fungsi" value="{{$sensors->fungsi}}" class="form-control">
    </div>
    @error('fungsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection