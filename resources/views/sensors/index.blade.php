@extends('layouts.master')

@section('judul')
    Daftar Sensor
@endsection

@push('script')
    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#tabelsensor").DataTable();
    });
    </script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

@auth
  <a href="/sensors/create" class="btn btn-secondary mb-3">Tambah Sensor</a>
@endauth

<table class="table" id="tabelsensor" class="table table-bordered table-striped">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Jenis</th>
        <th scope="col">Merk</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($sensors as $key => $sensor)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$sensor->jenis}}</td>
            <td>{{$sensor->merk}}</td>
            <td>
              @auth
               <form action="/sensors/{{$sensor->id}}" method="POST">
                <a href="/sensors/{{$sensor->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/sensors/{{$sensor->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
               </form>
               @endauth

               @guest
                <a href="/sensors/{{$sensor->id}}" class="btn btn-info btn-sm">Detail</a>
               @endguest
            </td>
        </tr>
     @empty
     <h1>Data tidak ditemukan</h1>
         
     @endforelse
    </tbody>
  </table>

@endsection