@extends('layouts.master')
@section('judul')
    <p>Detail Sensor</p>
@endsection

@section('content')
<div class="d-flex justify-content-center"">
    <h4>Detail Sensor</h4>
</div>
<table class="table table-bordered table-striped">
    <tbody>
        <tr>
            <th style="width: 20px">Jenis</th>
            <td>{{$sensor->jenis}}</td>
        </tr>
        <tr>
            <th>Merk</th>
            <td>{{$sensor->merk}}</td>
        </tr>
        <tr>
            <th>Fungsi</th>
            <td>{{$sensor->fungsi}}</td>
        </tr>
    </tbody>
</table>
<br><br>
<div class="d-flex justify-content-left"">
    <h5>Daftar gunungapi yang menggunakan sensor {{$sensor->jenis}}</h5>
</div>
<div class="row">
    @forelse ($sensor->volcanosensor as $item)
    <div class="col-4">
        <div class="card">
            <div class="card-body">
                {{-- <h5>{{$volcano->find($item->volcanos_id)['nama']}}</h5> --}}
                <h5>{{$volcano->where('id', $item->volcanos_id)->first()->nama}}</h5>
                <p class="card-text">Status sensor: 
                    @if ( $item->status == 1 ) 
                    Aktif
                    @else 
                    Tidak Aktif
                    @endif
                    <br>
                    Dipasang tahun: {{$item->year}}</p>
            </div>
            <form action="/profile/{{$item->volcanos_id}}" method="GET">
                <input type="submit" class="btn btn-info btn-sm btn-block" value="Detail Gunung">
            </form>
        </div>
    </div>
    @empty
    <h5>Data tidak ditemukan</h5>
    @endforelse
</div>
<br>
<div class="d-flex justify-content-end">
    <a href="/sensors" class="btn btn-primary mb-sm">Back</a>
</div>

@endsection