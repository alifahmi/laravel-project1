@extends('layouts.master')

@section('judul')
    Tambah Status
@endsection

@section('content')

<form action="/status" method="POST">
    @csrf
    
    <div class="form-group">
      <label>Status</label>
      <input type="text" name="status" class="form-control">
    </div>
    @error('status')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Info</label>
      <input type="text" name="info" class="form-control">
    </div>
    @error('info')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  
  @endsection