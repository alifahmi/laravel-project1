@extends('layouts.master')

@section('judul')
    Edit Status
@endsection

@section('content')

<form action="/status/{{$status->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
    <label>Status</label>
    <input type="text" name="status" value="{{$status->status}}" class="form-control">
    </div>
    @error('status')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Info</label>
    <input type="text" name="info" value="{{$status->info}}" class="form-control">
    </div>
    @error('info')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <button type="submit" class="btn btn-primary">Update</button>
</form>

@endsection