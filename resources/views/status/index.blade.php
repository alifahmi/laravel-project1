@extends('layouts.master')

@section('judul')
    Daftar Status
@endsection

@push('script')
    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#tabelstatus").DataTable();
    });
    </script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

@auth
  <a href="/status/create" class="btn btn-secondary mb-3">Tambah Status</a>
@endauth

<table id="tabelstatus" class="table table-bordered table-striped">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Status</th>
        <th scope="col">Info</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($status as $key => $status)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$status->status}}</td>
            <td>{{Str::limit($status->info)}}</td>
            <td>
              @auth
               <form action="/status/{{$status->id}}" method="POST">
                <a href="/status/{{$status->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/status/{{$status->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ask{{$key + 1}}">
                Delete
                </button>
                <div class="modal fade" id="ask{{$key + 1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>Apakah anda akan menghapus status "{{$status->status}}" ?</p>
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
               @endauth

               @guest
                <a href="/status/{{$status->id}}" class="btn btn-info btn-sm">Detail</a>
               @endguest
            </td>
        </tr>
     @empty
     <h1>Data tidak ditemukan</h1>
         
     @endforelse
    </tbody>
  </table>

@endsection