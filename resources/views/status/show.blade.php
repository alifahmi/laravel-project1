@extends('layouts.master')
@section('judul')
    <p>Detail Status</p>
@endsection
    
@section('content')
    <div class="d-flex justify-content-center"">
        <h4>Detail Data</h4>
    </div>
    <table class="table table-bordered table-striped">
        <tbody>
            <tr>
                <th style="width: 20px">Status</th>
                <td>{{$status->status}}</td>
            </tr>
            <tr>
                <th>Info</th>
                <td>{{$status->info}}</td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <div class="d-flex justify-content-left"">
        <h5>Daftar gunungapi yang memiliki status {{$status->status}}</h5>
    </div>
    <div class="row">
        @forelse ($volcano->where('statuses_id', $status->id) as $item)
        <div class="col-3">
            <div class="card">
                <div class="card-body">
                    <h5>{{$item->nama}}</h5>
                </div>
                <form action="/profile/{{$item->id}}" method="GET">
                    <input type="submit" class="btn btn-info btn-sm btn-block" value="Detail Gunung">
                </form>
            </div>
        </div>
        @empty
        <h5>Data tidak ditemukan</h5>
        @endforelse
    </div>
    <div class="d-flex justify-content-end">
        <a href="/status" class="btn btn-primary mb-sm">Back</a>
    </div>
@endsection