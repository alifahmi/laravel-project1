@extends('layouts.master')

@section('judul')
    Tambah Gunungapi
@endsection

@push('script')
<script src="https://cdn.tiny.cloud/1/r76ebw8jiapbixdiisux58gj9qoju0jfjmfzsanvsbu1fthm/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste advtable tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export pageembed permanentpen table',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
  </script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>    
<script>
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
</script>
@endpush

@push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@section('content')

<form action="/volcano" method="POST">
    @csrf
    
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    
    <div class="form-group">
      <label>Status</label>
      <select name="status" class="js-example-basic-single" style="width: 100% ">
        <option value="">-- Pilih Status --</option>
        @foreach ($status as $statusitem)
          <option value="{{$statusitem->id}}">{{$statusitem->status}}</option>
        @endforeach
      </select>
    </div>
    
    <div class="form-group">
      <label>Sejarah</label>
      <textarea name="sejarah" class="form-control"></textarea>
  </div>
  @error('sejarah')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
    
    <button type="submit" class="btn btn-primary">Simpan</button>
  </form>
  
  @endsection