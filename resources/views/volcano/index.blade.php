@extends('layouts.master')

@section('judul')
    Daftar Gunungapi
@endsection

@push('script')
    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#tabelstatus").DataTable();
    });
    </script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')

@auth
  <a href="/volcano/create" class="btn btn-secondary mb-3">Tambah Gunungapi</a>
@endauth

<table id="tabelstatus" class="table table-bordered table-striped">
    <thead class="thead-light">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Gunungapi</th>
        <th scope="col">Status</th>
        <th scope="col">Sejarah</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
     @forelse ($volcano as $key => $volcano)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$volcano->nama}}</td>
            <td>{{$volcano->statuses->status}}</td>
            <td>{{strip_tags(Str::limit($volcano->sejarah,30))}}</td>
            <td>
              @auth
               <form action="/volcano/{{$volcano->id}}" method="POST">
                <a href="/profile/{{$volcano->profiles->id}}" class="btn btn-info btn-sm mb-1"style="width: 100px">Detail</a><br>
                {{-- <a href="/volcano/{{$volcano->id}}" class="btn btn-info btn-sm">Detail</a> --}}
                <a href="/volcano/{{$volcano->id}}/edit" class="btn btn-warning btn-sm mb-1"style="width: 100px">Edit Volcano</a><br>
                <a href="/profile/{{$volcano->profiles->id}}/edit" class="btn btn-warning btn-sm mb-1"style="width: 100px">Edit Profiles</a><br>
                <a href="/volcano/{{$volcano->id}}" class="btn btn-warning btn-sm mb-1" style="width: 100px">Edit Sensors</a><br>
                <button type="button" style="width: 100px" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ask{{$key + 1}}">
                Delete
                </button>
                <div class="modal fade" id="ask{{$key + 1}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p>Apakah anda akan menghapus volcano "{{$volcano->nama}}" ?</p>
                        
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                      </form>
                      </div>
                    </div>
                  </div>
                </div>
               @endauth

               @guest
               <a href="/profile/{{$volcano->profiles->id}}" class="btn btn-info btn-sm mb-1"style="width: 100px">Detail</a><br>
               @endguest
            </td>
        </tr>
     @empty
     <h1>Data tidak ditemukan</h1>
         
     @endforelse
    </tbody>
  </table>

@endsection