@extends('layouts.master')
@section('judul')
    <p>Edit Sensor</p>
@endsection

@section('content')

<div class="row">
    @foreach ($volcano->volcanosensor as $item)
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                
                <h5><b>{{$item->sensor->jenis}}</b></h5>
                <p class="card-text">Tahun: {{$item->year}}<br>
                    @if ( $item->status == 1 ) 
                    Status: Aktif</p>
                    @else 
                    Status: Tidak Aktif</p>
                    @endif
            </div>
            <form action="/volcanosensor/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <input type="submit" class="btn btn-danger btn-sm btn-block" value="Delete">
            </form>
        </div>
    </div>
    @endforeach
</div>
    <div class="d-flex justify-content-left">
        <h4>Tambahkan Sensor</h4>
    </div>
    <form action="/volcanosensor" method="POST" enctype="multipart/form-data" class=my-3>
        @csrf
        <table class="table table-bordered table-striped">
            <tbody>
                <input type="hidden" name="volcanos_id" value={{$volcano->id}} class="form-control">
                <tr>
                    <th style="width: 20px">Nama</th>
                    <td><select name="sensor_id" class="form-control" placeholder="Nama Sensor">
                        <option value="">-- Pilih Sensor --</option>
                        @foreach ($sensor as $item)
                            <option value="{{$item->id}}">{{$item->jenis}}</option>
                        @endforeach
                    </select></td>
                </tr>
                <tr>
                    <th>Tahun</th>
                    <td><input type="number" name="year" class="form-control">
                    </td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><select name="status" class="form-control">
                        <option value="">-- Pilih Status --</option>
                        <option value="1">Aktif</option>
                        <option value="0">Tidak Aktif</option>
                    </select></td>
                </tr>
                <tr>
                   <td colspan="2"><button type="submit" class="btn btn-success">Submit</button></td>
                </tr>
            </tbody>
        </table>

        @error('volcanos_id')
        <div class="alert alert-danger">{{ $message }}</div>

        @error('sensor_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        @error('year')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        @error('status')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        @enderror
    </form>
    

    <div class="d-flex justify-content-end">
        <a href="/volcano" class="btn btn-primary mb-sm">Back</a>
    </div>
@endsection