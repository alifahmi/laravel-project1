<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@index');

// Route::get('/home', 'HomeController@index');

Route::resource('account', 'AccountController')->only(['index', 'update']);
Route::resource('sensors', 'SensorController');
Route::resource('status', 'StatusesController');
Route::resource('volcano', 'VolcanosController');
Route::resource('profile', 'ProfilesController');
Route::resource('volcanosensor', 'VolcanoSensorController');